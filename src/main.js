import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import { BootstrapVue } from "bootstrap-vue";
Vue.use(BootstrapVue);

import Verte from "verte";
import "verte/dist/verte.css";

Vue.component(Verte.name, Verte);

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import VueSlider from 'vue-slider-component';
import 'vue-slider-component/theme/material.css';

Vue.component('VueSlider', VueSlider);

import { library } from '@fortawesome/fontawesome-svg-core';
import { faPen, faTrashAlt, faCopy, faGripVertical } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPen, faTrashAlt, faCopy, faGripVertical);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = true;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
