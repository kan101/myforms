# myforms

# Form Builder - built with Vue. 

Form Builder is an application developed with Vue.js. The application functions as an interactive form builder that can handle all HTML form elements and allows for styling customization.

Upon completion of form building, Vue Form Builder outputs the corresponding HTML markup for further project integration. The application can cater to a range of form complexities, from simple to advanced forms.

I hope to improve this with time. 

test url: https://formakr.netlify.app/#/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
